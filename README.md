===============================================      
Compiling Apache from source, made easy   
http://immanuelnoel.com     
===============================================      
        
Script to compile Apache from source on Linux based machines. Should just work out of the box on Mac.  
Prerequisites: gcc, gcc-c++, unzip, tar libraries installed     
     
Copy the HTTPD, PCRE, APR, APR-UTIL archive files (*.tar.gz / *.zip) onto the same directory and run ./build.sh     
      
Tested script for Apache 2.2.x and 2.4.x on Linux.  
   
**Does not work on/if,**  
-> Solaris - If the tar command does not support -z flag. The solution is to edit the script, and replace (without quotes) "tar -zxvf $1" with "gunzip -c $1 | tar xvf -"