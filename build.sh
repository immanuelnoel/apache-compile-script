echo "========================================================"
echo "Compiling Apache from source, made easy"
echo "Prerequisites - GCC, GCC-C++ packages"
echo "https://bitbucket.org/immanuelnoel/apache-compile-script"
echo ""
echo "Modify Apache / PCRE Install Path if multiple versions need to co-exist"
echo "========================================================"

apacheInstallLocationTemp="/usr/local/apache"
read -p "Enter Apache Install Path (Default:$apacheInstallLocationTemp) : " apacheInstallLocation
apacheInstallLocation=${apacheInstallLocation:-$apacheInstallLocationTemp}

httpdTemp="httpd-2.4.9.tar.gz"
read -p "Enter HTTPD archive path (Default:$httpdTemp) : " httpd
httpd=${httpd:-$httpdTemp}

isSeries22Temp="N"
read -p "Is Apache Version 2.2.x - APR, APR-UTIL, PCRE are included with source, and installation will be skipped (Default:$isSeries22Temp) : " isSeries22
isSeries22=${isSeries22:-$isSeries22Temp}

apr=""
aprutil=""
pcre=""

# Skip APR, APR_UTIL, PCRE if Apache source has them included
if [ "$isSeries22" == "N" ]; then

	# Get APR Path
	aprTemp="apr-1.5.1.tar.gz"
	read -p "Enter APR library path (Default:$aprTemp) : " apr
	apr=${apr:-$aprTemp}

	# Get APR-UTIL Path
	aprutilTemp="apr-util-1.5.3.tar.gz"
	read -p "Enter APR-UTIL library path (Default:$aprutilTemp) : " aprutil
	aprutil=${aprutil:-$aprutilTemp}
	
	# Get PCRE Path
	pcreInstalledTemp="N"
	read -p "Is PCRE already installed (Default:$pcreInstalledTemp) : " pcreInstalled
	pcreInstalled=${pcreInstalled:-$pcreInstalledTemp}
	
	# Skip PCRE installation if already installed
	if [ "$pcreInstalled" == "N" ]; then
		pcreTemp="pcre-8.35.zip"
		read -p "Enter PCRE library path (Default:$pcreTemp) : " pcre
		pcre=${pcre:-$pcreTemp}
	fi
	
	# Get PCRE to_install / installed location
	pcreInstallLocationTemp="/usr/local/pcre"
	read -p "Enter PCRE install location (Default:$pcreInstallLocationTemp) : " pcreInstallLocation
	pcreInstallLocation=${pcreInstallLocation:-$pcreInstallLocationTemp}
	
else
	echo "Skipping APR, APR_UTIL, PCRE explicit installation. Apache source has them included"
fi


# Functions
filename=""
function extract() {
	filename="${1%.*}"
	extension="${1##*.}"
	if [ "$extension"="gz" ]; then
		filename="${filename%.*}"
		tar -zxvf $1
	fi
	if [ "$extension"="zip" ]; then
		unzip $1
	fi
	return $TRUE
}


# Extract HTTPD
extract $httpd
httpdDirectory=$filename

# Skip APR, APR_UTIL if Apache 2.2.x is being installed
if [ "$isSeries22" == "N" ]; then
	# Extract APR
	extract $apr
	mv $filename $httpdDirectory/srclib/
	mv $httpdDirectory/srclib/$filename $httpdDirectory/srclib/apr

	# Extract APR-UTIL
	extract $aprutil
	mv $filename $httpdDirectory/srclib/
	mv $httpdDirectory/srclib/$filename $httpdDirectory/srclib/apr-util
	
	# Install PCRE
	# Skip PCRE installation if already installed
	if [ "$pcreInstalled" == "N" ]; then
		extract $pcre
		cd "$filename"
		./configure --prefix=$pcreInstallLocation
		make
		make install
		cd ..
	fi
fi

# Install HTTPD
cd $httpdDirectory
# On solaris, to compile for 64 bit: run export CFLAGS="-m64"

if [ "$isSeries22" == "N" ]; then
	./configure --enable-file-cache --enable-cache --enable-disk-cache --enable-mem-cache --enable-expires --enable-headers --enable-usertrack --enable-cgi --enable-vhost-alias --enable-rewrite --enable-so --with-included-apr --with-pcre=$pcreInstallLocation --prefix=$apacheInstallLocation
else
	./configure --enable-file-cache --enable-cache --enable-disk-cache --enable-mem-cache --enable-expires --enable-headers --enable-usertrack --enable-cgi --enable-vhost-alias --enable-rewrite --enable-so --with-included-apr --prefix=$apacheInstallLocation
fi


make
make install